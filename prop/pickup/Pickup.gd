extends Area2D
class_name Pickup

# dependent on order in spritesheet, YEAH I KNOW
enum Kind {
	MISSILE,
	BULLETS,
	HEALTH,
	LASER,
	SHIELD,
	HYPER,
}

var kind setget set_kind
var amount = 1


func _ready():
	self.kind = randi() % Kind.size()


func set_kind(value):
	kind = value
	$Sprite.frame = int(value)
	match value:
		Kind.HEALTH, Kind.MISSILE:
			amount = 1
		Kind.BULLETS:
			amount = 6


func _process(delta):
	position += Vector2.DOWN * 35.0 * delta


func _on_Pickup_area_entered(_area):
	set_deferred("monitorable", false)
	hide()
	$Picked.play()


func _on_Picked_finished():
	queue_free()


func get_capture():
	var dict = {
		"name": "Pickup",
		"parent": get_parent().get_path(),
		"position_x": global_position.x,
		"position_y": global_position.y,
		"amount": amount,
		"kind": kind,
	}
	return dict


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
