extends Area2D

signal caught()


func _on_EnemyGoal_body_entered(enemy):
	$Caught.play()
	emit_signal("caught")
	enemy.queue_free() # is this ok?
