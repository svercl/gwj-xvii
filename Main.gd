extends Node

export(String, FILE, "*.tscn") var first_scene

onready var transition = $TopLevel/Transition
var _changing_scene = false

var _settings = {
	"master_volume": 100,
	"music_volume": 100,
	"sfx_volume": 100,
}
const SETTINGS_PATH = "user://settings.cfg"


func load_settings():
	var file = File.new()
	if !file.file_exists(SETTINGS_PATH):
		return
	var err = file.open(SETTINGS_PATH, File.READ)
	if err != OK:
		return
	var json = parse_json(file.get_line())
	file.close()
	if typeof(json) != TYPE_DICTIONARY:
		return
	_settings = json # :)

	_set_bus_volume("Master", _settings.master_volume)
	_set_bus_volume("Music", _settings.music_volume)
	_set_bus_volume("SFX", _settings.sfx_volume)


func save_settings():
	var file = File.new()
	var err = file.open(SETTINGS_PATH, File.WRITE)
	if err != OK:
		return
	var json = to_json(_settings)
	file.store_line(json)
	file.close()


func _get_bus_volume(bus_name):
	var volume_db = AudioServer.get_bus_volume_db(AudioServer.get_bus_index(bus_name))
	return min(db2linear(volume_db) * 100.0, 100.0)


func _set_bus_volume(bus_name, volume_db):
	volume_db = max(linear2db(volume_db / 100.0), -80.0)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index(bus_name), volume_db)


func _exit_tree():
	_settings.master_volume = _get_bus_volume("Master")
	_settings.music_volume = _get_bus_volume("Music")
	_settings.sfx_volume = _get_bus_volume("SFX")
	save_settings()

# Entrypoint into the game
func _ready():
	randomize()
	load_settings()
	save_settings()
	change_scene(first_scene)


func change_scene(path):
	assert(!path.empty())
	if _changing_scene:
		return
	_changing_scene = true
	call_deferred("_actual_change_scene", path)


func _actual_change_scene(path):
	transition.fade_out()
	yield(transition, "done")
	for child in $Scene.get_children():
		child.queue_free()
	var scene = load(path).instance()
	# allow them to switch scenes by this signal
	# almost like a state machine
	scene.connect("finished", self, "change_scene")
	$Scene.add_child(scene)
	transition.fade_in()
	_changing_scene = false
