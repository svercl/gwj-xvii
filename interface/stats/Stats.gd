extends Control

const VISIBLE_HEARTS = 4
const Heart = preload("res://interface/stats/Heart.tscn")

onready var hearts = $MarginContainer/HBoxContainer/Hearts
onready var heart_count = $MarginContainer/HBoxContainer/Hearts/Count
onready var rewind = $MarginContainer/HBoxContainer/Rewind

const RewindNo = preload("res://interface/stats/rewind-no.png")
const RewindYes = preload("res://interface/stats/rewind-yes.png")

# deferred
func _on_Player_health_changed(health):
	for child in hearts.get_children():
		if child is Label: # hack
			continue
		child.queue_free()
	if health > VISIBLE_HEARTS:
		hearts.add_child(Heart.instance())
		heart_count.text = str("+", health)
		return
	for _i in range(0, health):
		hearts.add_child(Heart.instance())
	heart_count.text = ""


func _on_Level_score_changed(score):
	$MarginContainer/HBoxContainer/Score.text = str("Score: ", score)


func _on_Rewind_new_capture():
	rewind.texture = RewindYes
	rewind.modulate = Color(255, 255, 255, 255)
	$Flash.start()


func _on_Flash_timeout():
	rewind.modulate = Color.white


func _on_Rewind_rewinded():
	rewind.texture = RewindNo
