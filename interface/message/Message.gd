extends CenterContainer


func _ready():
	hide()


func show_message(msg, duration):
	$Message.text = msg
	$Tween.interpolate_property(self, "modulate", Color(0, 0, 0, 0), Color.white, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_started")
	show()
	$Duration.start(duration)


func _on_Duration_timeout():
	$Tween.interpolate_property(self, "modulate", Color.white, Color(0, 0, 0, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	hide()


func _on_Level_show_message(msg):
	show_message(msg, 2.0)
