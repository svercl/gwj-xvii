extends HBoxContainer

export(String, "Master", "Music", "SFX") var bus_name
var bus_index

#
#func grab_focus():
#	# override so we can get focus on the slider, ignore everything else
#	$HSlider.grab_focus()


func _ready():
	$Name.text = bus_name
	bus_index = AudioServer.get_bus_index(bus_name)
	var volume_db = AudioServer.get_bus_volume_db(bus_index)
	# rounded by slider
	$HSlider.value = min(db2linear(volume_db) * 100.0, 100.0)


func _on_HSlider_value_changed(value):
	var volume_db = max(linear2db(value / 100.0), -80.0)
	AudioServer.set_bus_volume_db(bus_index, volume_db)
	$Value.text = str(value)
