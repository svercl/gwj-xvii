extends ColorRect

signal done()

export(float, 0.1, 1.0) var duration = 0.4


func fade_in():
	$Tween.interpolate_property(material, "shader_param/cutoff", 0.0, 1.0, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")


func fade_out():
	$Tween.interpolate_property(material, "shader_param/cutoff", 1.0, 0.0, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	yield($Tween, "tween_completed")
	emit_signal("done")
