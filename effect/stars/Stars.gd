extends ParallaxBackground

const FRONT_SPEED = 25.0
const MID_SPEED = 15.0
const BACK_SPEED = 5.0

onready var front = $Front
onready var mid = $Mid
onready var back = $Back


func _process(delta):
	front.motion_offset.y += FRONT_SPEED * delta
	mid.motion_offset.y += MID_SPEED * delta
	back.motion_offset.y += BACK_SPEED * delta
