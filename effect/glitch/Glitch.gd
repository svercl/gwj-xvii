extends ColorRect

const DISTORTION_INDEX = 0
onready var master_bus = AudioServer.get_bus_index("Master")


func _on_Glitch_visibility_changed():
	$Interference.playing = visible
	AudioServer.set_bus_effect_enabled(master_bus, DISTORTION_INDEX, visible)


# make sure to disable the effect when this leaves the tree
# very important
func _exit_tree():
	AudioServer.set_bus_effect_enabled(master_bus, DISTORTION_INDEX, false)
