extends KinematicBody2D

signal shoot_bullet(pos, speed)
signal shoot_missile(pos)
signal health_changed(health)
signal hit()
signal died()

const BULLET_COOLDOWN = 0.5
const HYPER_BULLET_COOLDOWN = 0.2

const ACCELERATION = 0.4
const FRICTION = 0.2

const MAX_BULLETS = 33
const MAX_MISSILES = 2
const MAX_HEARTS = 11

const MOVE_SPEED = 100.0
const BULLET_SPEED = 180.0
const MISSILE_SPEED = 50.0

var velocity = Vector2.ZERO

var health = 2 setget set_health
var bullets = 29 setget set_bullets
var missiles = 2 setget set_missiles

# corresponds to sprite frame
enum Frame {
	IDLE,
	RIGHT,
	LEFT,
}


func _ready():
	self.health = health
	self.bullets = bullets
	self.missiles = missiles


func set_health(value):
	value = min(value, MAX_HEARTS)
	health = value
	emit_signal("health_changed", value)


func set_bullets(value):
	# fixed size bullets
	value = min(value, MAX_BULLETS)
	bullets = value
	$Data/Ammo/Label.text = str(value)
	if value == 0:
		$Data/Ammo.modulate = Color("#e6da29") # yellow
	else:
		$Data/Ammo.modulate = Color.white


func set_missiles(value):
	value = min(value, MAX_MISSILES)
	missiles = value
	$Data/Missiles/Label.text = str(value)
	if value == 0:
		$Data/Missiles.modulate = Color("#e6da29") # yellow
	else:
		$Data/Missiles.modulate = Color.white


func _physics_process(delta):
	if $Cooldown.is_stopped() && Input.is_action_pressed("shoot_bullet"):
		$Cooldown.start()
		if bullets > 0:
			self.bullets -= 1
			$SFX/ShootBullet.play()
			emit_signal("shoot_bullet", $BulletSpawn.global_position, BULLET_SPEED)
		else:
			$SFX/Out.play()

	if $Cooldown.is_stopped() && Input.is_action_just_pressed("shoot_missile"):
		$Cooldown.start()
		if missiles > 0:
			self.missiles -= 1
			$SFX/ShootMissile.play()
			emit_signal("shoot_missile", $BulletSpawn.global_position)
		else:
			$SFX/Out.play()

	var motion = Vector2.ZERO

	if Input.is_action_pressed("left"):
		$Sprite.frame = Frame.LEFT
		motion.x = -MOVE_SPEED
	elif Input.is_action_pressed("right"):
		$Sprite.frame = Frame.RIGHT
		motion.x = MOVE_SPEED
	else:
		$Sprite.frame = Frame.IDLE

	if motion.length() != 0.0:
		velocity = velocity.linear_interpolate(motion, ACCELERATION)
	else:
		velocity = velocity.linear_interpolate(Vector2.ZERO, FRICTION)

	var _coll = move_and_collide(velocity * delta)


func take_damage(amount):
	if $Shield.visible:
		$SFX/Deflect.play()
		return
	emit_signal("hit")
	$SFX/Hit.play()
	self.health = max(health - amount, 0)
	if health == 0:
		emit_signal("died")


func _on_PickupDetector_area_entered(pickup):
	# we can assume some properties of the pickup
	var text = ""
	match pickup.kind:
		Pickup.Kind.MISSILE:
			text = "missile"
			self.missiles += pickup.amount
		Pickup.Kind.BULLETS:
			text = "bullets"
			self.bullets += pickup.amount
		Pickup.Kind.HEALTH:
			text = "health"
			self.health += pickup.amount
		Pickup.Kind.LASER:
			text = "laser"
			$Laser.show()
			$LaserCooldown.start()
		Pickup.Kind.SHIELD:
			text = "shield"
			$Shield.show()
			$ShieldCooldown.start()
		Pickup.Kind.HYPER:
			text = "hyper"
			$Cooldown.wait_time = HYPER_BULLET_COOLDOWN
			$FX.play("hyper")
			$HyperCooldown.start()
	$Pickup.show()
	$Pickup.text = str("got ", text)
	$Pickup/Shown.start()


func _on_Shown_timeout():
	$Pickup.hide()


func _on_LaserCooldown_timeout():
	$Laser.hide()


func _on_ShieldCooldown_timeout():
	$Shield.hide()


func _on_Regen_timeout():
	var old_bullets = bullets
	self.bullets += 1
	if bullets != old_bullets: # only if changed
		$SFX/Regen.play()


func _on_EnemyGoal_caught():
	take_damage(1)


func _on_HyperCooldown_timeout():
	$Cooldown.wait_time = BULLET_COOLDOWN
	$FX.stop()
	# sometimes can get stuck purple
	# send help
	modulate = Color.white
