extends Camera2D

const SHAKE_TRANS = Tween.TRANS_SINE
const SHAKE_EASE = Tween.EASE_IN_OUT


onready var noise = OpenSimplexNoise.new()
var noise_y = 0
var _amplitude = 0.0


func _ready():
	noise.seed = randi()
	noise.period = 4
	noise.octaves = 2


func shake(duration = 0.1, frequency = 15, amplitude = 5.0):
	_amplitude = amplitude
	$Duration.wait_time = duration
	$Frequency.wait_time = 1.0 / float(frequency)
	$Duration.start()
	$Frequency.start()

	new_shake()


func new_shake():
	noise_y += 1
	var new_offset = Vector2.ZERO
	new_offset.x = _amplitude * noise.get_noise_2d(noise.seed * 2, noise_y)
	new_offset.y = _amplitude * noise.get_noise_2d(noise.seed * 3, noise_y)
	$Tween.interpolate_property(self, "offset", offset, new_offset, $Frequency.wait_time, SHAKE_TRANS, SHAKE_EASE)
	$Tween.start()


func _on_Frequency_timeout():
	new_shake()


func _on_Duration_timeout():
	$Tween.interpolate_property(self, "offset", offset, Vector2.ZERO, $Frequency.wait_time, SHAKE_TRANS, SHAKE_EASE)
	$Tween.start()
	$Frequency.stop()


func _on_Player_hit():
	shake(0.2, 20, 6.0)


func _on_Enemy_hit():
	shake()

func _on_EnemyGoal_caught():
	shake(0.3, 30, 7.0)
