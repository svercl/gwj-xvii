extends Node2D

var current_wave = 1
var enemies_for_wave = 3
# keep track of how many enemies we've spawned as to not spawn too many
var spawned_enemies = 0
var score = 0 setget set_score

# connected to ~HUD/Stats~
signal score_changed(score)
# connected to ~HUD/Message~
signal show_message(msg)
# connected to ~Main~ (manages the scenes)
signal finished(path)

const Bullet = preload("res://projectile/Bullet.tscn")
const Missile = preload("res://projectile/Missile.tscn")
const Enemy = preload("res://actor/enemy/Enemy.tscn")
const Explosion = preload("res://effect/explosion/Explosion.tscn")
const Pickup = preload("res://prop/pickup/Pickup.tscn")


func _ready():
	# be polite
	# wait until everything is ready to go, then we can start :)
	call_deferred("start_wave")


# this is only necessary to reduce coupling,
# however we can just get the node path to the
# stats and update the label that way, but this
# allows us to not care about their node setup
func set_score(value):
	score = value
	emit_signal("score_changed", value)


func start_wave():
	emit_signal("show_message", "Prepare thyself")
	# small wait before actually starting, i intentionally avoid using
	# yield to prevent errors when switching scenes
	$StartingWait.start()


func _on_StartingWait_timeout():
	emit_signal("show_message", str("Begin wave ", current_wave))
	$EnemyTimer.start()


func _on_EnemyTimer_timeout():
	# we are done spawning enemies
	if spawned_enemies == enemies_for_wave:
		return
	spawned_enemies += 1
	# work with 3.2
	$EnemyPath/EnemySpawn.unit_offset = rand_range(0, 1)
	var enemy = Enemy.instance()
	$Enemies.add_child(enemy)
	enemy.position = $EnemyPath/EnemySpawn.position
	enemy.connect("shoot", self, "_on_Enemy_shoot")
	enemy.connect("died", self, "_on_Enemy_died")
	enemy.connect("tree_exited", self, "_on_Enemy_tree_exited")
	enemy.connect("hit", $Camera, "_on_Enemy_hit")


func _on_Enemy_died(pos, amount, color):
	self.score += amount
	# we always want a pickup and an explosion, because we have a finite
	# amount of ammo, missiles, and health
	var explosion = Explosion.instance()
	$Explosions.add_child(explosion)
	explosion.position = pos
	explosion.modulate = color

	if rand_range(0, 1) <= 0.2:
		return

	var pickup = Pickup.instance()
	$Pickups.add_child(pickup)
	pickup.position = pos


func _on_Enemy_tree_exited():
	if spawned_enemies == enemies_for_wave && $Enemies.get_child_count() == 0:
		end_wave()


func end_wave():
	emit_signal("show_message", str("Wave ", current_wave, " ended"))
	# we can go onto the next wave!
	current_wave += 1
	enemies_for_wave += 2
	spawned_enemies = 0
	$EnemyTimer.stop()
	# small wait for the same reason as in ~start_wave~
	$EndWait.start()


func _on_EndWait_timeout():
	start_wave()


func shoot(pos, speed, color):
	var bullet = Bullet.instance()
	$Projectiles.add_child(bullet)
	bullet.start(pos, speed)
	bullet.modulate = color


func _on_Enemy_shoot(pos, speed):
	# Color is from palette
	shoot(pos, Vector2.DOWN * speed, Color("#d32734"))


func _on_Player_shoot_bullet(pos, speed):
	shoot(pos, Vector2.UP * speed, Color("#e6da29"))


func _on_Player_shoot_missile(pos):
	var missile = Missile.instance()
	$Projectiles.add_child(missile)
	missile.position = pos


func _on_Player_died():
	emit_signal("finished", "res://scenes/lose/Lose.tscn")


func _on_Rewind_rewinded():
	# fix the connections
	for enemy in $Enemies.get_children():
		if !enemy.is_connected("shoot", self, "_on_Enemy_shoot"):
			enemy.connect("shoot", self, "_on_Enemy_shoot")
		if !enemy.is_connected("died", self, "_on_Enemy_died"):
			enemy.connect("died", self, "_on_Enemy_died")
		if !enemy.is_connected("tree_exited", self, "_on_Enemy_tree_exited"):
			enemy.connect("tree_exited", self, "_on_Enemy_tree_exited")
		if !enemy.is_connected("hit", $Camera, "_on_Enemy_hit"):
			enemy.connect("hit", $Camera, "_on_Enemy_hit")
	emit_signal("show_message", "Rewinded!")


func _on_Pause_menu_pressed():
	emit_signal("finished", "res://scenes/menu/Menu.tscn")
