extends Control

signal finished(path)


func _ready():
	$CenterContainer/VBoxContainer/Retry.grab_focus()
	$CenterContainer/VBoxContainer/Quit.visible = OS.get_name() != "HTML5"


func _on_Retry_pressed():
	emit_signal("finished", "res://scenes/level/Level.tscn")


func _on_Menu_pressed():
	emit_signal("finished", "res://scenes/menu/Menu.tscn")


func _on_Quit_pressed():
	get_tree().quit()
