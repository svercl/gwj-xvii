extends Control

export(String, FILE, "*.tscn") var next_scene = "res://scenes/menu/Menu.tscn"

signal finished(path)


func _ready():
	$Tween.interpolate_property($CenterContainer, "modulate", Color(0, 0, 0, 0), Color.white, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()


func _on_Timer_timeout():
	emit_signal("finished", next_scene)
