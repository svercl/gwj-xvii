extends Panel

signal finished(path)

const TITLE_COLORS = [
	Color("#e6da29"), # yellow
	Color("#2d93dd"), # blue
	Color("#28c641"), # green
	Color("#7b53ad"), # purple
	Color("#da7d22"), # orange
	Color("#d32734"), # red
	Color("#fdfdf8"), # white
]

onready var title = $CenterContainer/VBoxContainer/Title
onready var glitch = $CanvasLayer/Glitch
onready var play = $CenterContainer/VBoxContainer/Play
onready var quit = $CenterContainer/VBoxContainer/Quit


func _ready():
	title.text = ProjectSettings.get_setting("application/config/name")
	play.grab_focus()
	quit.visible = OS.get_name() != "HTML5"


func _on_Play_pressed():
	emit_signal("finished", "res://scenes/level/Level.tscn")


func _on_Options_pressed():
	emit_signal("finished", "res://scenes/options/Options.tscn")


func _on_Quit_pressed():
	get_tree().quit()


func _on_ColorSwitch_timeout():
	title.set("custom_colors/font_color", TITLE_COLORS[randi() % TITLE_COLORS.size()])


func _on_Glitch_timeout():
	glitch.visible = !glitch.visible
	$Glitch.start(rand_range(2.0, 3.0))
