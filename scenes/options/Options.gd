extends Panel

signal finished(path)


func _ready():
	$CenterContainer/VBoxContainer/MasterSlider.grab_focus()


func _on_Back_pressed():
	emit_signal("finished", "res://scenes/menu/Menu.tscn")
