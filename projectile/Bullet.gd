extends KinematicBody2D

var damage = 1
var velocity = Vector2.ZERO


func start(pos, speed, color = Color.white):
	global_position = pos
	velocity = speed
	modulate = color


func _physics_process(delta):
	var col = move_and_collide(velocity * delta)
	if col != null:
		if col.collider.has_method("take_damage"):
			col.collider.take_damage(damage)
		queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func get_capture():
	var dict = {
		"name": "Bullet",
		"parent": get_parent().get_path(),
		"position_x": global_position.x,
		"position_y": global_position.y,
		"velocity_x": velocity.x,
		"velocity_y": velocity.y,
		"modulate": modulate,
	}
	return dict
